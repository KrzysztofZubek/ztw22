<?php

namespace Album;

use Album\Model\Album;
use Album\Model\AlbumTable;
use Album\Model\Mailv;
use Album\Model\MailvTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Album\Model\AlbumTable' =>  function($sm) {
                    $tableGateway = $sm->get('AlbumTableGateway');
                    $table = new AlbumTable($tableGateway);
                    return $table;
                },
                'AlbumTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Album());
                    return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
                },
				
				'Album\Model\MailvTable' =>  function($sm) {
                    $tableGateway2 = $sm->get('MailvTableGateway');
                    $table2 = new MailvTable($tableGateway2);
                    return $table2;
                },
                'MailvTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Mailv());
                    return new TableGateway('mailv', $dbAdapter, null, $resultSetPrototype);
                },
            ),
			
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}