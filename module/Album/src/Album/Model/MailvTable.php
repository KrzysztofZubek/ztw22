<?php

namespace Album\Model;

use Zend\Db\TableGateway\TableGateway;

class MailvTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getMailv($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveMailv(Mailv $Mailv)
    {
        $data = array(
            'mail' => $Mailv->mailv,
        );
		
		echo $data;

        $id = (int)$Mailv->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getMailv($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteMailv($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
}