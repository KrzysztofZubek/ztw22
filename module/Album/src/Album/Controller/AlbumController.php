<?php

namespace Album\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Album\Model\Album;
use Album\Model\Mailv;
use Album\Form\AlbumForm;
use Album\Form\MailForm;
use Zend\Mail;
use Zend\Mail\Transport\Smtp;
use Zend\Soap\Client;



class AlbumController extends AbstractActionController
{
    protected $albumTable;
    protected $mailTable;
	protected $password; // = "P@ssw0rdZub3k";
	protected $userlogin; // = "kzub3k";
	protected $allegroid; // = "4c4b9af3";
	protected $mailaddress; // = "gam3s000@gmail.com";
	protected $mailpassword; // = "P@ssw0rdZub3k";
	protected $smtp; // = "smtp.gmail.com";
	
	
	public function parsIniFile(){
		$configini = parse_ini_file("config.ini",true);
		
		$this->password = $configini['allegro']['password'];
		$this->userlogin = $configini['allegro']['login'];
		$this->allegroid = $configini['allegro']['key'];
		
		$this->mailaddress = $configini['mail']['address'];
		$this->mailpassword = $configini['mail']['password'];
		$this->smtp = $configini['mail']['server'];
	}
	
	//Pobiera liste wszystkich aukcji z danym produktem
	public function getAllegroItems($id){
		//Pobiera album o konkretnym ID
		$album = $this->getAlbumTable()->getAlbum($id);
		
		//Tworzy obiekt SOAP, ktory bedzie komunikowac sie z Allegro API (WSDL)
		$client = new Client("https://webapi.allegro.pl/service.php?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT));
		//Zeby Allegro API dzialalo trzeba ustawic wersje silnika SOAP
		$client->setSoapVersion(SOAP_1_1 );

				
		//Lista argumentow dla funkcji pobierajacej aktualne aukcje
		$dogetitemslist_request = array(
			'webapiKey' => $this->allegroid,											//Kluczz wygenerowane dla konkretnej aplikacji allegro
			'countryId' => 1,															//Kraj = Polska
			'filterOptions' => array(
				array('filterId' => 'closed', 'filterValueId' => array('0')),			//Nie wyswietlaj zakonczonych
				array('filterId' => 'search', 'filterValueId' => array($album->title)),	//Filtrowanie aukcji po tytule produktu
					
			),
					
			'sortOptions' => null,	//Bez sortowania
			'resultSize' => 1000,	//Maksymalna ilosc wynikow
			'resultOffset' => 1,	
			'resultScope' => 0,
		);
		
		//Pobierz aukcje dla konkretnego produktu
		$retn = $client->doGetItemsList($dogetitemslist_request);

		//Ile aukcji znalazl
		$itemsCount = $retn->itemsCount;
		
		//Lista odnalezionych aukcji
		$foundItems = array();
			
		
		//Jezeli znalazl przynajmniej jedna oferte
		if($itemsCount>0){
			//Pobiera tablice z wszystkimi wynikami
			$items = $retn->itemsList->item;

			foreach($items as $item){
				//Zapisuje do osobnej tablicy poszczegolne informacje o aukcji
				$itemInfo = array(
					'itemId' => $item->itemId,								//Id przedmiotu
					'itemTitle' => $item->itemTitle,						//Tytul aukcji
					'leftCount' => $item->leftCount,						//Pozostala ilosc produktu
					'timeToEnd' => $item->timeToEnd,						//Ile czasu do konca aukcji
					'conditionInfo' => $item->conditionInfo,				//Stan produktu
					'priceType' => $item->priceInfo->item[0]->priceType,	//Rodzaj sprzedazy (kup teraz/licytacja)
					'priceValue' => $item->priceInfo->item[0]->priceValue,	//Cena
				);
				
				array_push($foundItems,$itemInfo);
			}
		}else{
				//Jezeli nie ma zadnych wynikow to zwraca pusta tablice
				$itemInfo = array(
					'itemId' => null,
					'itemTitle' => null,
					'leftCount' => null,
					'timeToEnd' => null,
					'conditionInfo' => null,
					'priceType' => null,
					'priceValue' => null,
				);
				
				array_push($foundItems,$itemInfo);			
		}	
		
		return $foundItems;
	}
	
	//Wysylanie maila na maile z listy z wszystkimi aukcjami
	public function sendAllegroAction(){
		//Pobranie i zapisanie w zmiennych hasla loginu do allegro i maila
		$this->parsIniFile();
		
		//Pobranie zapytania do serwera
		$request = $this->getRequest();
		//Pobranie id produktu
		$id = (int) $this->params()->fromRoute('id', 0);

		//Jezeli POST
        if ($request->isPost()) {

			//Pobranie czy uzytkownik zgodzil sie na usuniecie wpisu
            $del = $request->getPost('send', 'Nie');

			//Jezeli tak
            if ($del == 'Tak') {
				//Pobranie wszystkie maile
				$mails = $this->getMailTable()->fetchAll();
				
				//Porabnie aukcji danego produktu
				$foundItems = $this->getAllegroItems($id);
				
				//Tresc maila
				$mailBody = "<h1><center>Lista ofert</center></h1><table><tr><th>ID przedmiotu</th><th>Tytul</th><th>Cena</th><th>Do konca</th></tr>";
				
				foreach ($foundItems as $item){
																	//Link do aukcji o danym ID 											
					$mailBody = $mailBody . "<tr>" . "<td><a href=\"http://allegro.pl/show_item.php?item=" . $item['itemId'] . "\" target=\"_blank\"/>" . $item['itemId'] . 
					"</a></td><td>" . $item['itemTitle'] . "</td><td>" . $item['priceValue'] . "</td><td>" . $item['timeToEnd'] . "</td>";
					
					
					$mailBody = $mailBody . "</tr>";
					
				}
				
				$mailBody = $mailBody . "</table>";
				
				
				$message = new \Zend\Mail\Message();
				$message->setFrom($this->mailaddress);
				
				foreach ($mails as $mail){
					$message->addTo($mail->mailv);
				}
				
				$message->setSubject('Lista ofert z allegro');
				
				$bodyPart = new \Zend\Mime\Message();
				$bodyMessage = new \Zend\Mime\Part($mailBody);
				$bodyMessage->type = 'text/html';
				$bodyPart->setParts(array($bodyMessage));
				$message->setBody($bodyPart);
				$message->setEncoding('UTF-8');

				$smtpOptions = new \Zend\Mail\Transport\SmtpOptions();  
				$smtpOptions->setHost($this->smtp)
					->setConnectionClass('login')
					->setName($this->smtp)
					->setConnectionConfig(array(
						'username' => $this->mailaddress,
						'password' => $this->mailpassword,
						'ssl' => 'tls',
				));

				$transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
				$transport->send($message);
            }

            // Redirect to list of albums
			return $this->redirect()->toRoute('album');
        }		
		
		return new ViewModel(array(
				'id' => $id,
		));		
	}
	
	//Wyswietla informacje o aktualnie zalogowanym uzytkowniku allegro
	public function getAllergoUserInfoAction(){
		//Pobranie i zapisanie w zmiennych hasla loginu do allegro i maila
		$this->parsIniFile();		 
		 
		$client = new Client("https://webapi.allegro.pl/service.php?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT));
		$client->setSoapVersion(SOAP_1_1 );
		 
		$result = $client->call(
			'doQueryAllSysStatus',
			array(
				array(
					'countryId' => 1,
					'webapiKey' => $this->allegroid,
				)
			)
		);
		 
		$versionKeys = array();
		$dd=0;
		
		
		foreach ($result->sysCountryStatus->item as $row) {
			$versionKeys[$row->countryId] = $row;
		}
		 
		$session = $client->call(
			'doLoginEnc',
			array(
				array(
					'userLogin' => $this->userlogin,
					'userHashPassword' =>  base64_encode(hash('sha256', $this->password, true)),
					'countryCode' => 1,
					'webapiKey' => $this->allegroid,
					'localVersion' => $versionKeys[1]->verKey
				)
			)
		);
		
		$dogetmydata_request = array(
			'sessionHandle' => $session->sessionHandlePart,
		);
		
		$userdata = $client->doGetMyData($dogetmydata_request);
		
		$userinfo = array(
			'userId' => $userdata->userData->userId,
			'userLogin' => $userdata->userData->userLogin,
			'userFirstName' => $userdata->userData->userFirstName,
			'userLastName' =>  $userdata->userData->userLastName,
			'userEmail' => $userdata->userData->userEmail,
		);
		
		return new ViewModel(array(
				'userinfo' => $userinfo,
		));		
				
	}
	
	//Wyswietla wszystkie aukcje 
	public function getAllegroAction(){		
		//Pobranie i zapisanie w zmiennych hasla loginu do allegro i maila
		$this->parsIniFile();

		//Pobranie ID wpisu
		$id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
			//Jezeli ID jest nieprawidlowe to wchodzi na glowna strone
            return $this->redirect()->toRoute('album');
        }
		
		$foundItems = $this->getAllegroItems($id);
		

		return new ViewModel(array(
				'items' => $foundItems,
				'id' => $id,
		));		
	}

    public function indexAction()
    {				
		$this->parsIniFile();
		//Pobranie wszystkich albumow
        return new ViewModel(array(
            'albums' => $this->getAlbumTable()->fetchAll(),
        ));
    }
	
	public function viewMailAction(){
		//Pobranie wszystkich maili
		return new ViewModel(array(
            'mails' => $this->getMailTable()->fetchAll(),
        ));
	}
	
	//Dodaje maila do listy
	public function addMailAction(){
		$form = new MailForm();
		$form->get('submit')->setValue('Add');
		
		//Pobranie zapytania wyslanego do serwera
        $request = $this->getRequest();
		
		//Sprawdzenie czy post
        if ($request->isPost()) {
			//Utworzenie obiektu reprezentujacego wpis w bazie danych
            $mail = new Mailv();
			//Dodanie do formy filtru zdefiniowanego w obiekcie Album
            $form->setInputFilter($mail->getInputFilter());
			//Pobranie danych z zapytania POST
            $form->setData($request->getPost());
			
			//Sprawdzenie czy wprowadzone dane sa poprawne
            if ($form->isValid()) {		
				
				//Jezeli tak zapisuje dane do bazy
                $mail->exchangeArray($form->getData());
                $this->getMailTable()->saveMailv($mail);

                // Powrot na strone z lista gier
                return $this->redirect()->toRoute('album');
            }
        }
		
		return array('form' => $form);
	
	}

	//Edycja maila z listy
	public function editMailAction(){
		//Pobranie ID wpisu
		$id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
			//Jezeli ID jest nieprawidlowe to wchodzi na glowna strone
            return $this->redirect()->toRoute('album');
        }

		//Pobranie mailu o konkretnym id
        $mail = $this->getMailTable()->getMailv($id);

		//Utworzenie formy z mailem
        $form  = new MailForm();
        $form->bind($mail);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($mail->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
				//Zapisuje w bazie zmieniony adres
                $this->getMailTable()->saveMailv($form->getData());

                // Redirect to list of albums
                return $this->redirect()->toRoute('album');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
	}
	
	//Usuniecie maila z listy
	public function delMailAction(){
		$id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
			//Pobranie czy uzytkownik zgodzil sie na usuniecie wpisu
            $del = $request->getPost('del', 'Nie');

			//Jezeli tak
            if ($del == 'Tak') {
				//Pobiera wpis
                //$id = (int) $request->getPost('id');
				
				//Usuwa maila z bazy
                $this->getMailTable()->deleteMailv($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('album');
        }

        return array(
            'id'    => $id,
            'mail' => $this->getMailTable()->getMailv($id)
        );
	}
	
	//Kod wysylajacy maile z lista 
	public function sendMailAction(){
		//Pobranie i zapisanie w zmiennych hasla loginu do allegro i maila
		$this->parsIniFile();

		
		//Pobranie zapytania do serwera
		$request = $this->getRequest();
        if ($request->isPost()) {
			//Pobranie czy uzytkownik zgodzil sie na usuniecie wpisu
            $del = $request->getPost('del', 'No');

			//Jezeli tak
            if ($del == 'Yes') {
				//Pobranie wszystkich maili z listy
				$mails = $this->getMailTable()->fetchAll();
				//Pobranie wszystkich elementow z tablicy produktow
				$album = $this->getAlbumTable()->fetchAll();
				
				//$mailBody jest tresc maila (HTML) ktory zostanie wyslany na maile z listy
				$mailBody = "<h1><center>Lista gier</center></h1><table>";
				
				//Listuje wszystkie produkty z bazy i tworzy maila 
				foreach ($album as $game){
					$mailBody = $mailBody . "<hr>";
																	//Tytul produktu
					$mailBody = $mailBody . "<h5>Tytul:</h5><h3>" . $game->title . "</h3>".
											 //Tworca/Producent
					"<h5>Wydawca:</h5><h3>". $game->artist . "</h3>".
										  //Opis produktu
					"<h5>Opis:</h5><h3>". $game->text . "</h3>";
					
					//Urle z linkami do obrazkow
					$urls = $game->urls;
					
					$urls = $game->urls;
		
					//Filtr na URLe. 
					$pattern = '(\[http\:\/\/[a-zA-Z0-9\.\/\#\$\%\&\@\?\=\+\-\;\'\"\:\,\_\:]*\])';
					//Wyszukuje wszystkie prawidlowe adresy URL
					preg_match_all($pattern, $urls, $matches);
				
					//Jezeli uzytownik podal przynajmniej jeden url prawidlowo znajdzie sie w $_urls
					foreach($matches[0] as $match){
						//Jezeli wpis posiada przynajmniej jeden prawidlowy URL to dodaje obrazek z tego URL.
						if(strlen($match)>0){
							$mailBody = $mailBody . "<img src=\"" . substr($match,1,strlen($match)-2) . "\"\>"; 
						}
					}
				
					//Zamkniecie wpisu w tabelce
					$mailBody = $mailBody . "</hr>";
					
				}
				
				//Zamkniecie tabelki
				$mailBody = $mailBody . "</table>";
				
				$message = new \Zend\Mail\Message();
								   //Adres mail, na ktory zostanie wyslany mail
				$message->setFrom($this->mailaddress);
				
				//Listuje wszystkie maile w bazie
				foreach ($mails as $mail){
									//Dodaje kolejne adresy, do ktorych ma zostac wyslany mail
					$message->addTo($mail->mailv);
				}
				
									 //Tytul maila
				$message->setSubject('Lista produktow');
				
				$bodyPart = new \Zend\Mime\Message();
				$bodyMessage = new \Zend\Mime\Part($mailBody);
				$bodyMessage->type = 'text/html';
										  //Tworzenie tresci maila
				$bodyPart->setParts(array($bodyMessage));
				$message->setBody($bodyPart);
				$message->setEncoding('UTF-8');

				$smtpOptions = new \Zend\Mail\Transport\SmtpOptions();  
									  //Ustawienie serwera smtp
				$smtpOptions->setHost($this->smtp)
										 //Rodzaj autoryzacji
					->setConnectionClass('login')
					->setName($this->smtp)
					->setConnectionConfig(array(
						'username' => $this->mailaddress,		//Adres mail z ktorego jest wysylany mail
						'password' => $this->mailpassword,			//Haslo do konca mail
						'ssl' => 'tls',
				));

				$transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
				$transport->send($message);		//Wyslanie maila
            }

            // Redirect to list of albums
			return $this->redirect()->toRoute('album');
        }		
	}
	
	//Wyswietla szczegolowe informacje o wpisie w bazie
	public function viewAction(){
		//Pobranie ID wpisu
		$id = (int) $this->params()->fromRoute('id', 0);
		
		//Jezeli ID nie istnieje to przejdz do formy tworzacej nowy wpis
		if (!$id) {
            return $this->redirect()->toRoute('album', array(
                'action' => 'add'
            ));
        }
		
		//Pobranie wpisu o konkretnym ID
		$albums = $this->getAlbumTable()->getAlbum($id);
		//Pobranie adresow do sceenow
		$urls = $albums->urls;
		
		//Filtr regex, ktory wyciaga adresy URL z formy
		$pattern = '(\[http\:\/\/[a-zA-Z0-9\.\/\#\$\%\&\@\?\=\+\-\;\'\"\:\,\_\:]*\])';
		preg_match_all($pattern, $urls, $matches);
		
		$_urls = array();
		
		//Jezeli uzytownik podal przynajmniej jeden url prawidlowo znajdzie sie w $_urls
		foreach($matches[0] as $match){
			$turls = $match;
			array_push($_urls,substr($turls,1,strlen($turls)-2));
		}
		
		//Zwraca album o konkretnym ID oraz tablice prawidlowych URL
        return new ViewModel(array(
            'album' => $albums,
			'urls'  => $_urls,
			
        ));
		
	}

    public function addAction()
    {
		//Utworzenie nowej formy
        $form = new AlbumForm();
        $form->get('submit')->setValue('Dodaj');

		//Pobranie zapytania wyslanego do serwera
        $request = $this->getRequest();
		
		//Sprawdzenie czy post
        if ($request->isPost()) {
			//Pobranie argumentu urls
			$urls = $request->getPost("urls");
			
			//Patern sprawdzajacy czy lista url jest prawidlowa
			$pattern = '(\[http\:\/\/[a-zA-Z0-9\.\/\#\$\%\&\@\?\=\+\-\;\'\"\:\,\_\:]*\])';
			preg_match_all($pattern, $urls, $matches);
			//var_dump($matches);
		
			$_urls = "";
		
			//Jezeli uzytownik podal przynajmniej jeden url prawidlowo znajdzie sie w $_urls
			foreach($matches[0] as $match){
				$_urls = $_urls.$match;
			}
			
			if ($request->getPost()->offsetExists('urls')) {
				//Zmiana parametru urls na przefiltrowane dane
				$request->getPost()->offsetSet('urls',$_urls);
			}


			//Utworzenie obiektu reprezentujacego wpis w bazie danych
            $album = new Album();
			//Dodanie do formy filtru zdefiniowanego w obiekcie Album
            $form->setInputFilter($album->getInputFilter());
			//Pobranie danych z zapytania POST
            $form->setData($request->getPost());

			//Sprawdzenie czy wprowadzone dane sa poprawne
            if ($form->isValid()) {
				//Jezeli tak zapisuje dane do bazy
                $album->exchangeArray($form->getData());
                $this->getAlbumTable()->saveAlbum($album);

                // Powrot na strone z lista gier
                return $this->redirect()->toRoute('album');
            }
        }
		
		//Zwraca obiekt formy, ktory zostanie wyswietlony na stronie
        return array('form' => $form);
    }

    public function editAction()
    {
		//Pobranie id wpisu
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
			//Jezeli nieprawidlowe przenosi na strone dodawania nowego wpisu
            return $this->redirect()->toRoute('album', array(
                'action' => 'add'
            ));
        }
		
		//Pobraie wpisu o konkretnym id
        $album = $this->getAlbumTable()->getAlbum($id);

		//Utworzenie nowego formularza
        $form  = new AlbumForm();
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

		//Pobranie zapytania do serwera
        $request = $this->getRequest();
		//Jezeli post
        if ($request->isPost()) {
			//Ustawia filtr, ktorym sprawdza czy dane sa prawidlowe
            $form->setInputFilter($album->getInputFilter());
			//Ustawia dane w formie
            $form->setData($request->getPost());
		
			//Jezeli dane w formie sa poprawne to zmienia je w bazie danych
            if ($form->isValid()) {
                $this->getAlbumTable()->saveAlbum($form->getData());

                //Powrot do strony glownej
                return $this->redirect()->toRoute('album');
            }
        }

		//Zwraca ID wpisu oraz forme ktora zostanie wyswietlona
        return array(
            'id' => $id,
            'form' => $form,
        );
    }
	
	//Usuwa wpis w bazie z grami
    public function deleteAction()
    {
		//Pobiera id konkretnego wpisu z zapytania
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
			//Jezeli id jest nieprawidlowe przekierowuje na strone glowna
            return $this->redirect()->toRoute('album');
        }

		//Pobiera zapytanie do serwera
        $request = $this->getRequest();
		
		//Jezeli POST
        if ($request->isPost()) {
			//Pobiera forme Tak/Nie na stronie
            $del = $request->getPost('del', 'Nie');

			//Jezeli TAK to pobiera z zapytania POST ID i usuwa wpis
            if ($del == 'Tak') {
				//Pobranie argumentu ID
                $id = (int) $request->getPost('id');
				//Usuwa wpis w bazie
                $this->getAlbumTable()->deleteAlbum($id);
            }

            //Powrot na strone glowna
            return $this->redirect()->toRoute('album');
        }

		//Zwraca ID wpisu oraz wpis do tabeli z grami o konkretnym ID
        return array(
            'id'    => $id,
            'album' => $this->getAlbumTable()->getAlbum($id)
        );
    }
	
	public function getAlbumTable()
    {
        if (!$this->albumTable) {
            $sm = $this->getServiceLocator();
            $this->albumTable = $sm->get('Album\Model\AlbumTable');
        }
        return $this->albumTable;
    }

    public function getMailTable()
    {
        if (!$this->mailTable) {
            $sm = $this->getServiceLocator();
            $this->mailTable = $sm->get('Album\Model\MailvTable');
        }
        return $this->mailTable;
    }
}